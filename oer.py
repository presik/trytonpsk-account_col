import datetime
import logging
import os
import sqlite3

import requests
from trytond.config import config


def get_connection():
    path_currencies = os.path.expanduser('~') + '/currencies'
    db_file = os.path.join(path_currencies, 'currency.db')

    conn = sqlite3.connect(db_file)
    return conn


def table_exists(cursor, table_name):
    cursor.execute("""
        SELECT name
        FROM sqlite_master
        WHERE type='table' AND name=?;
    """, (table_name,))
    result = cursor.fetchone()
    return result is not None


def get_and_store_currencies(cursor):

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS currencies (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            date_currency DATE,
            base TEXT,
            currency TEXT,
            rate REAL
        )
    """)
    apikey = config.get('exchange_rate', 'openexchangerates')
    if not apikey:
        raise Exception('No existe apikey en configuracion: exchange_rate.openexchangerates')
    url = f"https://openexchangerates.org/api/latest.json?app_id={apikey}"
    response = requests.get(url)
    data = response.json()
    for currency, rate in data['rates'].items():
        cursor.execute("INSERT INTO currencies (date_currency, base, currency, rate) VALUES (?, ?, ?, ?)",
                       (datetime.date.today(), data['base'], currency, rate))


def get_rates(currency='COP', date=None):
    path_currencies = os.path.expanduser('~') + '/currencies'
    records = {}
    try:
        conn = sqlite3.connect(path_currencies + '/currency.db')
        cursor = conn.cursor()
        if not table_exists(cursor, 'currencies'):
            get_and_store_currencies(cursor)
        result = get_rates_from_db(cursor, currency, date)
        if not result:
            get_and_store_currencies(cursor)
            result = get_rates_from_db(cursor, currency, date)
        if result:
            records['USD'] = 1 / result[4]
    except Exception:
        logging.exception('error get rates from database')
    return records


def get_rates_from_db(cursor, currency='COP', date=None):
    query = f"SELECT * FROM currencies WHERE date_currency='{date}' AND currency='{currency}';"
    cursor.execute(query)
    result = cursor.fetchone()
    return result
