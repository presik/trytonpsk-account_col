# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


from trytond.i18n import gettext
from trytond.model import ModelView, Workflow
from trytond.modules.account.exceptions import (
    FiscalYearCloseError,
)
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class FiscalYear(metaclass=PoolMeta):
    "Fiscal Year"
    __name__ = 'account.fiscalyear'

    def get_deferral(self, account, method):
        "Computes deferrals for accounts"
        pool = Pool()
        Currency = pool.get('currency.currency')
        Deferral = pool.get('account.account.deferral')

        if not account.type:
            return
        if not account.deferral:
            if not Currency.is_zero(self.company.currency, account.balance):
                raise FiscalYearCloseError(
                    gettext('account'
                        '.msg_close_fiscalyear_account_balance_not_zero',
                        account=account.rec_name))
        else:
            deferral = Deferral()
            deferral.account = account
            deferral.fiscalyear = self
            deferral.debit = account.debit
            deferral.credit = account.credit
            deferral.method = method
            deferral.amount_second_currency = account.amount_second_currency
            return deferral

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, fiscalyears):
        """
        Close a fiscal year
        """
        pool = Pool()
        Period = pool.get('account.period')
        Account = pool.get('account.account')
        Deferral = pool.get('account.account.deferral')
        transaction = Transaction()
        database = transaction.database
        connection = transaction.connection

        # Lock period to be sure no new period will be created in between.
        database.lock(connection, Period._table)
        deferrals = []
        for fiscalyear in fiscalyears:
            if cls.search([
                        ('end_date', '<=', fiscalyear.start_date),
                        ('state', '=', 'open'),
                        ('company', '=', fiscalyear.company.id),
                        ]):
                raise FiscalYearCloseError(
                    gettext('account.msg_close_fiscalyear_earlier',
                        fiscalyear=fiscalyear.rec_name))

            periods = Period.search([
                    ('fiscalyear', '=', fiscalyear.id),
                    ])
            Period.close(periods)

            with Transaction().set_context(fiscalyear=fiscalyear.id,
                    date=None, cumulate=True, journal=None, colgaap=True):
                accounts = Account.search([
                        ('company', '=', fiscalyear.company.id),
                        ('type', '!=', None),
                        ])
                deferrals += [_f for _f in (fiscalyear.get_deferral(a, 'colgaap')
                        for a in accounts) if _f]

            with Transaction().set_context(fiscalyear=fiscalyear.id,
                    date=None, cumulate=True, journal=None, ifrs=True):
                accounts = Account.search([
                        ('company', '=', fiscalyear.company.id),
                        ('type', '!=', None),
                        ])
                deferrals += [_f for _f in (fiscalyear.get_deferral(a, 'ifrs')
                        for a in accounts) if _f]
        Deferral.save(deferrals)
