# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import csv
import os

from sql import Null
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import StateTransition, Wizard

dir_module = os.path.dirname(os.path.realpath(__file__))


class Country(metaclass=PoolMeta):
    __name__ = 'country.country'
    dian_code = fields.Char('DIAN Code')


class PostalCode(metaclass=PoolMeta):
    __name__ = 'country.postal_code'
    dian_code = fields.Char('DIAN Code')


class Subdivision(metaclass=PoolMeta):
    __name__ = 'country.subdivision'
    dian_code = fields.Char('DIAN Code')
    cities = fields.One2Many('country.city', 'subdivision', 'Cities')

    @classmethod
    def search_rec_name(cls, name, clause):
        res = [
            'OR',
            ('dian_code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]
        return res


class City(ModelSQL, ModelView):
    "City"
    __name__ = 'country.city'
    name = fields.Char('Name')
    dian_code = fields.Char('DIAN Code')
    subdivision = fields.Many2One('country.subdivision', 'Subdivision',
        required=True)
    subdivision_city = fields.Char('Subdivision City')

    @classmethod
    def __register__(cls, module_name):
        sql_table = cls.__table__()
        super().__register__(module_name)
        cursor = Transaction().connection.cursor()
        cursor.execute(*sql_table.select(sql_table.id,
                where=sql_table.subdivision_city != Null))

        rowcount = cursor.rowcount
        if rowcount <= 0 or rowcount is None:
            query = """
                update country_city
                set subdivision_city=country_subdivision.dian_code || country_city.dian_code
                from country_subdivision
                where country_city.subdivision = country_subdivision.id
                and country_city.subdivision_city is null
                and country_city.dian_code is not null"""
            cursor.execute(query)

    @property
    def city_code(self):
        return self.subdivision.dian_code + self.dian_code

    @classmethod
    def search_rec_name(cls, name, clause):
        res = [
            'OR',
            ('subdivision_city',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]
        return res


class MatchSync(Wizard):
    "Match Sync Countries"
    __name__ = 'country.sync_match'
    start_state = 'sync'
    sync = StateTransition()

    def _get_file(self, namefile):
        _file = os.path.join(dir_module, 'data', namefile)
        fnames = ['name', 'code', 'dian_code']
        with open(_file) as csv_file:
            data = csv.DictReader(csv_file, fieldnames=fnames)
            match_codes = {}
            for row in data:
                match_codes[row['code']] = row['dian_code']
            return match_codes

    def _sync_countries(self):
        Country = Pool().get('country.country')
        data = self._get_file('match_countries.csv')
        countries = Country.search([
            ('code', 'in', data.keys()),
        ])
        for country in countries:
            Country.write([country], {'dian_code': data[country.code]})

    def _sync_subdivisions(self):
        Sub = Pool().get('country.subdivision')
        data = self._get_file('match_subdivisions.csv')
        subdivisions = Sub.search([
            ('code', 'in', data.keys()),
        ])
        for sub in subdivisions:
            Sub.write([sub], {'dian_code': data[sub.code]})

    def _create_cities(self):
        pool = Pool()
        City = pool.get('country.city')
        cities = City.search([])
        if cities:
            return

        Sub = pool.get('country.subdivision')
        _file = os.path.join(dir_module, 'data/cities.csv')
        fnames = ['name', 'code', 'dian_code']
        subdivisions = Sub.search_read([
            ('country.code', '=', 'CO'),
            ], fields_names=['code'])
        subs_dict = {sub['code']: sub['id'] for sub in subdivisions}
        to_create = []
        with open(_file) as csv_file:
            data = csv.DictReader(csv_file, fieldnames=fnames)
            next(data)
            for row in data:
                to_create.append({
                    'name': row['name'],
                    'subdivision': subs_dict[row['code']],
                    'dian_code': row['dian_code'],
                })
        if to_create:
            City.create(to_create)

    def _update_geocodes(self):
        pool = Pool()
        CountryGeo = pool.get('party.country_code')
        Country = pool.get('country.country')
        countries_geo = CountryGeo.search([])
        countries = Country.search_read([], fields_names=['dian_code'])
        _countries = {_ct['dian_code']: _ct['id'] for _ct in countries}
        for country in countries_geo:
            country_id = _countries.get(country.code, None)
            if country_id:
                CountryGeo.write([country], {'country_iso': country_id})

    def _update_parties(self):
        cursor = Transaction().connection.cursor()
        query_country = """
            UPDATE party_address AS pa SET country=co.country_id FROM (
                SELECT cc.id AS country_id, pco.id AS country_code
                    FROM country_country AS cc
                    INNER JOIN party_country_code AS pco ON pco.code=cc.dian_code
                ) AS co
                WHERE pa.country_code=co.country_code AND country IS NULL
        """
        cursor.execute(query_country)
        query_sub = """
            UPDATE party_address AS pa SET subdivision=co.subdivision_id FROM (
              SELECT cc.id AS subdivision_id, pco.id AS department_code
                FROM country_subdivision AS cc
                INNER JOIN party_department_code AS pco ON pco.code=cc.dian_code
            ) AS co
            WHERE pa.department_code=co.department_code AND subdivision IS NULL;
        """
        cursor.execute(query_sub)
        query_city = """
            UPDATE party_address AS pa SET city_co=a.city_id
              FROM (
                SELECT
                  ci.id AS city_id,
                  ci.name AS name_a,
                  cs.dian_code || ci.dian_code AS xcode
                  FROM country_city AS ci
                  INNER JOIN country_subdivision AS cs ON cs.id=ci.subdivision
              ) a
              INNER JOIN (
                SELECT
                  pcc.id AS city_id,
                  pcc.name AS name_b,
                  pdc.code || pcc.code AS xcode
                  FROM party_city_code AS pcc
                  INNER JOIN party_department_code AS pdc ON pdc.id=pcc.department
              ) b ON a.xcode=b.xcode
              WHERE pa.city_code=b.city_id AND pa.city_co IS NULL;
        """
        cursor.execute(query_city)

    def transition_sync(self):
        self._sync_countries()
        self._sync_subdivisions()
        self._create_cities()
        # Remove in future, when party.country_code
        self._update_geocodes()
        self._update_parties()
        return 'end'


class MatchSyncPostalCodes(Wizard):
    "Match Sync PostalCodes"
    __name__ = 'country.postal_codes.sync_match'
    start_state = 'sync'
    sync = StateTransition()

    def _get_file(self, namefile):
        _file = os.path.join(dir_module, 'data', namefile)
        fnames = ['codigo_postal', 'dian_code']
        with open(_file) as csv_file:
            data = csv.DictReader(csv_file, fieldnames=fnames)
            match_codes = {}
            for row in data:
                match_codes[row['codigo_postal']] = row['dian_code']
            return match_codes

    def _sync_postal_codes(self):
        PostalCode = Pool().get('country.postal_code')
        data = self._get_file('match_city_postal_code.csv')
        postal_codes = PostalCode.search([
            ('postal_code', 'in', data.keys()),
        ])
        to_write = []
        for postal_code in postal_codes:
            to_write.append([postal_code])
            to_write.append({'dian_code': data[postal_code.postal_code]})
        PostalCode.write(*to_write)

    def _update_subdivision_postal_codes(self):
        cursor = Transaction().connection.cursor()
        query_country = """
            UPDATE country_postal_code 
                SET subdivision = s.subdivision
            FROM (SELECT 
                    s.id AS subdivision,
                    s.dian_code,
                    p.postal_code,
                    p.id, p.city
                FROM country_subdivision AS s
                JOIN country_postal_code AS p ON s.dian_code = substring(p.postal_code, 1, 2)
                WHERE s.dian_code is not NULL) AS s
            WHERE s.id = country_postal_code.id;
        """
        cursor.execute(query_country)

    def _update_parties(self):
        cursor = Transaction().connection.cursor()
        query_city = """
            UPDATE party_address AS pa SET city=co.city FROM (
                SELECT c.id AS city_id, p.city
                    FROM (select DISTINCT dian_code, city, subdivision from country_postal_code) AS p
                    INNER JOIN country_city AS c ON c.subdivision = p.subdivision
                    AND c.dian_code = p.dian_code
                ) AS co
                WHERE pa.city_co=co.city_id AND pa.city IS NULL
        """
        cursor.execute(query_city)

    def transition_sync(self):
        # Remove in future
        self._sync_postal_codes()
        self._update_subdivision_postal_codes()
        self._update_parties()
        return 'end'
