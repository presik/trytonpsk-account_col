# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.i18n import gettext
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.pyson import Eval, Bool, PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
# from trytond.modules.account.account import BalanceSheetContext
from trytond.exceptions import UserError

_ZERO = Decimal('0.0')


class Impairment(ModelSQL, ModelView):
    "Impairment"
    __name__ = 'account.impairment'

    cutoff_date = fields.Date('Cutoff Date')
    company = fields.Many2One('Company')


class ImpairmentLine(ModelSQL, ModelView):
    "Impairment Line"
    __name__ = 'account.impairment.line'

    origin = fields.Many2One("account.invoice", "Invoice")
    invoice_date = fields.Function(fields.Date("Invoice Date"), 'get_date_invoice')
    maturity_date = fields.Date('Maturity Date')
