# from trytond.model import fields, ModelView, ModelSQL
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition


class UserLoginAttempRemove(Wizard):
    'Remove Login Attemp User'
    __name__ = 'res.user.remove_loggin_attemp'
    start_state = 'remove_login'
    remove_login = StateTransition()

    def transition_remove_login(self):
        LoginAttemp = Pool().get('res.user.login.attempt')
        logins_error = LoginAttemp.search([])
        LoginAttemp.delete(logins_error)
        return 'end'
