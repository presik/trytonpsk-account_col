import logging

from trytond.modules.currency.currency import CronFetchError
from trytond.pool import PoolMeta

from .oer import get_rates

logger = logging.getLogger(__name__)

# from trytond.ir import rate_decimal


class Cron(metaclass=PoolMeta):
    "Currency Cron"
    __name__ = 'currency.cron'

    @classmethod
    def __setup__(cls):
        super(Cron, cls).__setup__()
        new_select = [
                ('oer', 'Open Exchange Rates'),
        ]
        if new_select not in cls.source.selection:
            cls.source.selection.extend(new_select)

    def fetch_oer(self, date):
        print('fetch oer validate')
        try:
            return get_rates(self.currency.code, date)
        except Exception as e:
            raise CronFetchError() from e
