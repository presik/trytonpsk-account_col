# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from dateutil.relativedelta import relativedelta
from jinja2 import Template as Jinja2Template
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

from . import invoice

STATES = {
    'readonly': Eval('state') != 'draft',
}

_TYPE = [
    ('out', 'Customer'),
    ('in', 'Supplier'),
]

TYPE_INVOICE_OUT = invoice.TYPE_INVOICE_OUT
TYPE_INVOICE_IN = invoice.TYPE_INVOICE_IN


class InvoiceAuthorization(ModelSQL, ModelView):
    "Invoice Authorization"
    __name__ = 'account.invoice.authorization'
    number = fields.Char('Number Authorization', required=True, states=STATES)
    software_provider_id = fields.Char('Software Provider Id',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
            'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
        })
    start_date_auth = fields.Date('Start Date Auth', required=True,
        states=STATES)
    end_date_auth = fields.Date('End Date Auth', required=True, states=STATES)
    from_auth = fields.Integer('From Auth', required=True, states=STATES)
    to_auth = fields.Integer('To Auth', required=True, states=STATES)
    sequence = fields.Many2One('ir.sequence.strict', 'Sequence', required=True,
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES)
    kind = fields.Selection('get_invoice_type', 'Kind',
        states={
            'required': True,
        })
    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'State')
    kind_string = kind.translated('kind')
    name = fields.Char('Name', states=STATES)
    type = fields.Selection(_TYPE, 'Type', required=True,
         states=STATES)

    @classmethod
    def __setup__(cls):
        super(InvoiceAuthorization, cls).__setup__()

    @staticmethod
    def default_type():
        return 'out'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    def get_rec_name(self, name):
        name = self.name or ''
        name = name + ' [' + self.number + ']'
        return name

    @fields.depends('type')
    def get_invoice_type(self):
        types = TYPE_INVOICE_OUT
        if self.type and self.type == 'in':
            types = TYPE_INVOICE_IN
        return types

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        dom = [bool_op,
            ('number',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ('kind',) + tuple(clause[1:]),
        ]
        return dom

    def sequence_get(self):
        self.check_authorization()
        return self.sequence.get()

    def check_authorization(self):
        Date = Pool().get('ir.date')
        today = Date.today()
        invoice_type = ['1', '2', '3', '4', '05', 'P']
        if self.kind in invoice_type and (self.end_date_auth < today or self.sequence.number_next > self.to_auth):
            raise UserError(gettext('account_col.msg_auth_expired'))

    @classmethod
    def validate_expired_authorization(cls):
        pool = Pool()
        invoice_type = ['1', '2', '3', '4', '05', 'P']
        auths = cls.search([
            ('state', '=', 'active'),
            ('kind', 'in', invoice_type),

        ])
        today_ = pool.get('ir.date').today()
        delta = today_ - relativedelta(days=1)
        Invoice = pool.get('account.invoice')
        to_expired = []
        for auth in auths:
            invoices = Invoice.search_count([
                ('invoice_date', '>=', delta),
                ('authorization', '=', auth.id),
                ])
            next_number = auth.sequence.number_next
            res_days = (auth.end_date_auth - today_).days
            number_auth = (int(auth.to_auth) - int(next_number))
            value = {
                'resolution': None,
                'msg': '',
            }
            if res_days <= 0:
                value['resolution'] = auth
                value['msg'] += 'la autorización está vencida \n'
            elif res_days <= 10:
                value['resolution'] = auth
                value['msg'] += f'La autorización esta próxima a vencer, quedan {res_days!s} días \n'
            if next_number > auth.to_auth:
                value['resolution'] = auth
                value['msg'] += 'la operación supera el rango de numeración de la autorización \n'
            elif (int(number_auth) - int(next_number)) <= invoices:
                value['resolution'] = auth
                value['msg'] += 'Los rangos de numeración de la autorización estan proximos a vencer'
            if value.get('resolution'):
                to_expired.append(value)

        if to_expired:
            Template = pool.get('email.template')
            ModelData = pool.get('ir.model.data')
            auth = to_expired[0]['resolution']
            template_id = ModelData.get_id('account_col', 'email_template_expired_authorization_invoice')
            template = Template(template_id)
            template_jinja = Jinja2Template(template.html_body)
            template.html_body = template_jinja.render({'record': to_expired})
            Template.send(template, auth)
