UPDATE party_address AS pa
SET
    country = co.country_id
FROM
    (
        SELECT
            cc.id AS country_id,
            pco.id AS country_code
        FROM
            country_country AS cc
            INNER JOIN party_country_code AS pco ON pco.code = cc.dian_code
    ) AS co
WHERE
    pa.country_code = co.country_code
    AND country IS NULL;

UPDATE party_address AS pa
SET
    subdivision = co.subdivision_id
FROM
    (
        SELECT
            cc.id AS subdivision_id,
            pco.id AS department_code
        FROM
            country_subdivision AS cc
            INNER JOIN party_department_code AS pco ON pco.code = cc.dian_code
    ) AS co
WHERE
    pa.department_code = co.department_code
    AND subdivision IS NULL;

UPDATE party_address AS pa
SET
    city_co = a.city_id
FROM
    (
        SELECT
            ci.id AS city_id,
            ci.name AS name_a,
            cs.dian_code || ci.dian_code AS xcode
        FROM
            country_city AS ci
            INNER JOIN country_subdivision AS cs ON cs.id = ci.subdivision
    ) a
    INNER JOIN (
        SELECT
            pcc.id AS city_id,
            pcc.name AS name_b,
            pdc.code || pcc.code AS xcode
        FROM
            party_city_code AS pcc
            INNER JOIN party_department_code AS pdc ON pdc.id = pcc.department
    ) b ON a.xcode = b.xcode
WHERE
    pa.city_code = b.city_id
    AND pa.city_co IS NULL;

SELECT
    id,
    code,
    type
FROM
    account_account
WHERE
    company = 1
    AND "type" IS NOT NULL
    AND code > '41'
    AND code <= '70';

-- move_general
SELECT
    mv.number AS number,
    mv.date AS date_,
    pa.name AS party,
    pa.id_number AS id_number,
    ac.code AS account,
    ml.reference AS reference,
    ml.description AS desc,
    mv.origin_number AS origin_number,
    LEAST(ml.debit, aa.debit) AS debit,
    LEAST(ml.credit, aa.credit) AS credit,
    ml.base AS base,
    an.name AS analytic,
    oc.name AS oc
FROM
    account_move_line AS ml
    JOIN party_party AS pa ON pa.id = ml.party
    JOIN account_move AS mv ON ml.move = mv.id
    JOIN account_account AS ac ON ac.id = ml.account
    LEFT JOIN analytic_account_line AS aa ON aa.move_line = ml.id
    LEFT JOIN analytic_account_account AS an ON an.id = aa.account
    LEFT JOIN company_operation_center AS oc ON oc.id = ml.operation_center
WHERE
    mv.company = 1
    AND mv.period >= 37
    -- AND ml.account IN (1299, 1362)
ORDER BY mv.date ASC

SELECT
    mv.id,
    mv.origin_id AS origin_id,
    otable.number AS origin_number
FROM
    (
        SELECT
            id,
            SPLIT_PART (origin, ',', 1) AS model,
            SPLIT_PART (origin, ',', 2)::INTEGER AS origin_id
        FROM
            account_move AS move
        WHERE
            origin IS NOT NULL
            AND origin ILIKE 'account.invoice%'
    ) AS mv
    JOIN account_invoice AS otable ON otable.id = mv.origin_id
WHERE
    mv.model = 'account.invoice';


UPDATE account_move AS am
    SET origin_number=tx.number
FROM
    (
        SELECT
            am.id,
            ot.number AS number
        FROM
            account_invoice AS ot
        JOIN account_move AS am ON am.origin = CONCAT ('account.invoice', ',', ot.id)
        WHERE am.origin_number IS NULL AND origin IS NOT NULL
    ) AS tx
WHERE
    am.id = tx.id;

SELECT
    ml.description,
    ml.debit,
    ml.credit,
    lt.amount AS tax_amount,
    tx.rate,
    tx.type,
    ROUND(lt.amount / tx.rate, 2) AS base
FROM
    account_move_line AS ml
JOIN account_tax_line AS lt ON lt.move_line=ml.id
JOIN account_tax AS tx ON tx.id=lt.tax
WHERE lt.type = 'tax' AND tx.type='percentage';

UPDATE
    account_move_line AS li
    SET base=tt.base
FROM (
    SELECT
        ml.id,
        CASE
            WHEN tx.rate!=0 THEN ROUND(lt.amount / tx.rate, 2) ELSE 0
        END AS base
    FROM
        account_move_line AS ml
    INNER JOIN account_tax_line AS lt ON lt.move_line=ml.id
    JOIN account_tax AS tx ON tx.id=lt.tax
    WHERE lt.type = 'tax' AND tx.type='percentage' AND ml.base IS NULL
    ) AS tt
WHERE tt.id=li.id
