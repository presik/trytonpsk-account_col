
cash_flow_days = """
    WITH line_ids AS (SELECT l.id, Coalesce(l.maturity_date, m.date) AS date FROM account_move_line AS l
        LEFT JOIN account_move as m ON l.move=m.id
        RIGHT JOIN "account_cashflow-account_account" AS c ON c.account = l.account
        WHERE Coalesce(l.maturity_date, m.date) >= '{start_date}'
        AND Coalesce(l.maturity_date, m.date) <= '{end_date}'
        AND l.reconciliation is null
        )
    SELECT
        f2.name AS parent_name,
        f.name AS name,
        f.type_,
        a.code AS account_code,
        a.name AS account_name,
        line_ids.date AS date,
        SUM(l.debit - l.credit) AS total
    FROM
        account_move_line AS l
        RIGHT JOIN line_ids ON line_ids.id = l.id
        LEFT JOIN account_move AS m ON l.move = m.id
        LEFT JOIN account_account AS a ON l.account = a.id
        LEFT JOIN "account_cashflow-account_account" AS af ON af.account = l.account
        LEFT JOIN account_account_cashflow AS f ON f.id = af.cash_flow
        LEFT JOIN account_account_cashflow AS f2 ON f2.id = f.parent
    WHERE
        f.kind = 'normal'
    GROUP BY
       f2.sequence, f.sequence, f2.name, f.name, f.type_, a.code, a.name, line_ids.date
    ORDER BY f2.sequence, f.sequence, a.code
"""


cash_flow_weeks = """
    WITH line_ids AS (SELECT l.id, Coalesce(l.maturity_date, m.date) AS date FROM account_move_line AS l
        LEFT JOIN account_move as m ON l.move=m.id
        RIGHT JOIN "account_cashflow-account_account" AS c ON c.account = l.account
        WHERE Coalesce(l.maturity_date, m.date) >= '{start_date}'
        AND Coalesce(l.maturity_date, m.date) <= '{end_date}'
        AND l.reconciliation is null
    )
    SELECT
        f2.name AS parent_name,
        f.name AS name,
        f.type_,
        a.code AS account_code,
        a.name AS account_name,
        DATE_TRUNC('week', line_ids.date)::date AS date,
        SUM(l.debit - l.credit) AS total
    FROM
        account_move_line AS l
        RIGHT JOIN line_ids ON line_ids.id = l.id
        LEFT JOIN account_move AS m ON l.move = m.id
        LEFT JOIN account_account AS a ON l.account = a.id
        LEFT JOIN "account_cashflow-account_account" AS af ON af.account = l.account
        LEFT JOIN account_account_cashflow AS f ON f.id = af.cash_flow
        LEFT JOIN account_account_cashflow AS f2 ON f2.id = f.parent
    WHERE
        f.kind = 'normal'
    GROUP BY
        f2.sequence, f.sequence, f2.name, f.name, f.type_, a.code, a.name, DATE_TRUNC('week', line_ids.date)
    ORDER BY
        f2.sequence, f.sequence, a.code, date;
"""


accounts_target = """
    SELECT
        id
    FROM
        account_account
    WHERE
        company = {company}
        AND "type" IS NOT NULL
        {codes}
"""


move_general = """
SELECT
    mv.number AS number,
    pa.name AS party,
    pa.id_number,
    mv.date AS date_,
    ac.code AS account,
    ml.reference AS reference,
    ml.description AS concept,
    mv.description AS description,
    LEAST(ml.debit, aa.debit) AS debit,
    LEAST(ml.credit, aa.credit) AS credit,
    mv.origin_number AS origin_number,
    ml.base AS base,
    an.code AS analytic_code,
    an.name AS analytic,
    oc.name AS oc
FROM
    account_move_line AS ml
    JOIN party_party AS pa ON pa.id = ml.party
    JOIN account_move AS mv ON ml.move = mv.id
    JOIN account_account AS ac ON ac.id = ml.account
    LEFT JOIN analytic_account_line AS aa ON aa.move_line = ml.id
    LEFT JOIN analytic_account_account AS an ON an.id = aa.account
    LEFT JOIN company_operation_center AS oc ON oc.id = ml.operation_center
WHERE
    mv.company = {company}
    {periods}
    {accounts}
ORDER BY mv.date ASC
"""

def update_origin(rtable, model):
    return f"""
    UPDATE account_move AS am
        SET origin_number=tx.number
    FROM
        (
            SELECT
                am.id,
                ot.number AS number
            FROM
                {rtable} AS ot
            JOIN account_move AS am ON am.origin = CONCAT ('{model}', ',', ot.id)
            WHERE am.origin_number IS NULL AND am.origin IS NOT NULL
        ) AS tx
    WHERE
        am.id = tx.id
    """
