# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from sql import Null


class CityCode(ModelSQL, ModelView):
    "City Code"
    __name__ = "party.city_code"
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    department = fields.Many2One('party.department_code', 'Department')
    department_city = fields.Char('Department City')

    @classmethod
    def __setup__(cls):
        super(CityCode, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        sql_table = cls.__table__()
        super().__register__(module_name)
        cursor = Transaction().connection.cursor()
        cursor.execute(*sql_table.select(sql_table.id,
                where=sql_table.department_city != Null))

        rowcount = cursor.rowcount
        if rowcount <= 0 or rowcount is None:
            query = '''update party_city_code set department_city= party_department_code.code || party_city_code.code from party_department_code where party_city_code.department = party_department_code.id'''
            cursor.execute(query)

    @classmethod
    def search_rec_name(cls, name, clause):
        res = [
            'OR',
            ('department_city',) + tuple(clause[1:]),
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]
        return res

    @fields.depends('department', 'code')
    def on_change_code(self, name=None):
        if self.code and self.department.code:
            self.department_city = self.department.code + self.code
        else:
            self.department_city = None


class CountryCode(ModelSQL, ModelView):
    "Country Code"
    __name__ = "party.country_code"
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    country_iso = fields.Many2One('country.country', 'Country ISO Code')

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]


class DepartmentCode(ModelSQL, ModelView):
    "Department Code"
    __name__ = "party.department_code"
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)

    @classmethod
    def __setup__(cls):
        super(DepartmentCode, cls).__setup__()

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ]
