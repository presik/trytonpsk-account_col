# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from sql import Table
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import fields
from trytond.modules.account_product.product import account_used, template_property
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction


class ProductIdentifier(metaclass=PoolMeta):
    "Product Identifier"
    __name__ = 'product.identifier'

    @classmethod
    def __setup__(cls):
        super(ProductIdentifier, cls).__setup__()
        cls.type.selection.extend([
            ('hs_co', 'Products Tariff Heading CO'),
            ('unspsc', 'United Nations Standard Products and Services Code (UNSPSC)'),
            ])


class PriceList(metaclass=PoolMeta):
    __name__ = 'product.price_list'

    @classmethod
    def delete(cls, records):
        for record in records:
            if record.lines:
                raise UserError(
                    gettext('account_col.msg_dont_delete_price_list'))
        super(PriceList, cls).delete(records)


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    extra_tax = fields.Numeric('Extra Tax', digits=(16, 2))
    account_income_for_others_used = template_property('account_income_for_others_used')
    account_expense_diference_used = template_property('account_expense_diference_used')
    account_depreciation_diference_used = template_property('account_depreciation_diference_used')
    account_return_purchase_used = template_property('account_return_purchase_used')
    account_return_sale_used = template_property('account_return_sale_used')

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        cls.description.translate = False

    @classmethod
    def get_amount_with_extra_tax(cls, lines):
        tax_amount = []
        for line in lines:
            if line.product and hasattr(line.product, 'extra_tax') and line.product.extra_tax:
                tax_amount.append(line.product.extra_tax * Decimal(line.quantity))
        return sum(tax_amount)

    def get_rec_name(self, name=None):
        if self.code:
            return '[' + self.code + '] ' + self.name
        else:
            return self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Product, cls).search_rec_name(name, clause)
        return domain + [('code', clause[1], clause[2]), ('description', clause[1], clause[2])]


class ProductCategory(metaclass=PoolMeta):
    __name__ = 'product.category'
    account_return_sale = fields.MultiValue(fields.Many2One('account.account',
        'Account Return Sale', domain=[
            [
                'OR',
                ('type.revenue', '=', True),
                ('type.debt', '=', True),
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting']))
    account_return_purchase = fields.MultiValue(fields.Many2One('account.account',
        'Account Return Purchase', domain=[
            [
                'OR',
                ('type.expense', '=', True),
                ('type.debt', '=', True),
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting']))
    account_depreciation_diference = fields.MultiValue(fields.Many2One('account.account',
            'Account Depreciation Diference', domain=[
                ('type.fixed_asset', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                    | Eval('account_parent')
                    | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting']))
    account_expense_diference = fields.MultiValue(fields.Many2One('account.account',
            'Account Expense Diference', domain=[
                ('closed', '!=', True),
                ('type.expense', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                    | Eval('account_parent')
                    | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting']))

    account_income_for_others = fields.MultiValue(fields.Many2One('account.account',
        'Account Income for Others', domain=[
            ('closed', '!=', True),
            ['OR',
                ('type.revenue', '=', True),
                ('type.payable', '=', True),
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting']))
    # [TODO] fields in this module for model product.category for remove in database migrated to multivalue model

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {
            'account_return_sale', 'account_return_purchase',
            'account_depreciation_diference', 'account_expense_diference',
            'account_income_for_others'}:
            return pool.get('product.category.account')
        return super(ProductCategory, cls).multivalue_model(field)

    @property
    @account_used('account_income_for_others')
    def account_income_for_others_used(self):
        pass

    @property
    @account_used('account_expense_diference')
    def account_expense_diference_used(self):
        pass

    @property
    @account_used('account_depreciation_diference')
    def account_depreciation_diference_used(self):
        pass

    @property
    @account_used('account_return_purchase')
    def account_return_purchase_used(self):
        pass

    @property
    @account_used('account_return_sale')
    def account_return_sale_used(self):
        pass


class CategoryAccount(metaclass=PoolMeta):
    "Category Account"
    __name__ = 'product.category.account'

    account_return_sale = fields.Many2One('account.account',
        'Account Return Sale', domain=[
            [
                'OR',
                ('type.revenue', '=', True),
                ('type.debt', '=', True),
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting'])
    account_return_purchase = fields.Many2One('account.account',
        'Account Return Purchase', domain=[
            [
                'OR',
                ('type.expense', '=', True),
                ('type.debt', '=', True),
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting'])
    account_depreciation_diference = fields.Many2One('account.account',
            'Account Depreciation Diference', domain=[
                ('type.fixed_asset', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                    | Eval('account_parent')
                    | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting'])
    account_expense_diference = fields.Many2One('account.account',
            'Account Expense Diference', domain=[
                ('closed', '!=', True),
                ('type.expense', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                    | Eval('account_parent')
                    | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting'])

    account_income_for_others = fields.Many2One('account.account',
        'Account Income for Others', domain=[
            ('closed', '!=', True),
            ['OR',
                ('type.revenue', '=', True),
                ('type.payable', '=', True),
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting'])

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        exist = table.column_exist('account_return_sale')

        super(CategoryAccount, cls).__register__(module_name)

        # if not exist:
        #     cls.migrate_columns()

    @classmethod
    def migrate_columns(cls):
        cursor = Transaction().connection.cursor()
        category_table = Table('product_category_account')
        query = """SELECT id, account_return_sale, account_return_purchase,
                account_depreciation_diference, account_expense_diference
                FROM product_category
                WHERE
                    account_return_sale IS NOT NULL OR
                    account_return_purchase IS NOT NULL OR
                    account_depreciation_diference IS NOT NULL OR
                    account_expense_diference IS NOT NULL;"""
        cursor.execute(query)
        result = cursor.fetchall()

        acc_categories = cls.search([])
        dict_acc_categories = {acc.category.id: acc for acc in acc_categories}
        to_write = []
        for c in result:
            if c[0] in dict_acc_categories:
                cat_multivalue = dict_acc_categories[c[0]]
                to_write.append([cat_multivalue])
                columns = [
                    category_table.account_return_sale,
                    category_table.account_return_purchase,
                    category_table.account_depreciation_diference,
                    category_table.account_expense_diference,
                    ]
                values = [c[1], c[2], c[3], c[4]]
                cursor.execute(*category_table.update(
                    columns=columns,
                    values=values,
                    where=category_table.id == cat_multivalue.id,
                ))
            else:
                print('esta debe ser creada', c)


class ProductTemplate(metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def __setup__(cls):
        super(ProductTemplate, cls).__setup__()
        cls.name.translate = False
        cls.categories.domain.append(('accounting', '!=', True))
        # cls.account_category.states = {
        #     'required': True,
        # }

    @property
    @account_used('account_income_for_others', 'account_category')
    def account_income_for_others_used(self):
        pass

    @property
    @account_used('account_expense_diference', 'account_category')
    def account_expense_diference_used(self):
        pass

    @property
    @account_used('account_depreciation_diference', 'account_category')
    def account_depreciation_diference_used(self):
        pass

    @property
    @account_used('account_return_purchase', 'account_category')
    def account_return_purchase_used(self):
        pass

    @property
    @account_used('account_return_sale', 'account_category')
    def account_return_sale_used(self):
        pass

    @staticmethod
    def default_exportable():
        return False
