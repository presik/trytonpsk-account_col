# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard
from trytond.report import Report
from .helper import polar_query
from .queries import accounts_target, move_general


class GeneralMoveStart(ModelView):
    "General Move Start"
    __name__ = 'account_col.print_general_move.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '<=', (Eval('end_period'), 'start_date')),
            ], depends=['fiscalyear', 'end_period'])
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '>=', (Eval('start_period'), 'start_date')),
            ],
        depends=['fiscalyear', 'start_period'])
    start_account = fields.Many2One('account.account', 'Start Account',
            domain=[
                ('type', '!=', None),
                ('code', '!=', None),
            ])
    end_account = fields.Many2One('account.account', 'End Account',
            domain=[
                ('type', '!=', None),
                ('code', '!=', None),
            ])
    start_code = fields.Char('Start Code Account')
    end_code = fields.Char('End Code Account')
    party = fields.Many2One('party.party', 'Party')
    company = fields.Many2One('company.company', 'Company', required=True)
    posted = fields.Boolean('Posted Move', help='Show only posted move')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), test_state=False).id

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_posted():
        return False

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class GeneralMove(Wizard):
    "Print General Move"
    __name__ = 'account_col.print_general_move'
    start = StateView('account_col.print_general_move.start',
        'account_col.print_general_move_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ],
    )
    print_ = StateReport('account_col.general_move')

    def _search_records(self):
        pass

    def do_print_(self, action):
        data = self.start
        start_period = data.start_period.id if data.start_period else None
        end_period = data.end_period.id if data.end_period else None
        party = data.party.id if data.party else None
        start_account = data.start_account.code if data.start_account else None
        end_account = data.end_account.code if data.end_account else None

        data = {
            'ids': [],
            'company': data.company.id,
            'fiscalyear': data.fiscalyear.id,
            'fiscalyear_name': data.fiscalyear.name,
            'start_period': start_period,
            'end_period': end_period,
            'start_account': start_account,
            'end_account': end_account,
            'party': party,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class GeneralMoveReport(Report):
    __name__ = 'account_col.general_move'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        pool = Pool()
        Period = pool.get('account.period')
        Company = pool.get('company.company')
        company = Company(data['company'])

        dom_accounts = [
            ('company', '=', data['company']),
            ('type', '!=', None),
        ]

        codes_target = ''
        if data['start_account']:
            dom_accounts.append(
                ('code', '>', data['start_account']),
            )
            codes_target += f" AND code > '{data['start_account']}'"
        if data['end_account']:
            dom_accounts.append(
                ('code', '<=', data['end_account']),
            )
            codes_target += f" AND code <= '{data['end_account']}'"

        accounts = ''
        if codes_target:
            args = {
                'company': data['company'],
                'codes': codes_target,
            }
            df_accounts = polar_query(accounts_target, args)
            accounts = tuple(acc[0] for acc in df_accounts.rows())

        records = None
        start_periods = set()
        start_period = None
        if data['start_period']:
            start_period = Period(data['start_period'])
            start_periods = Period.search_read([
                    ('fiscalyear', '=', data['fiscalyear']),
                    ('start_date', '>=', start_period.start_date),
                ])
            start_periods = set(p['id'] for p in start_periods)

        end_periods = set()
        end_period = None
        if data['end_period']:
            end_period = Period(data['end_period'])
            end_periods = Period.search_read([
                    ('fiscalyear', '=', data['fiscalyear']),
                    ('end_date', '<=', end_period.end_date),
                ])
            end_periods = set(p['id'] for p in end_periods)

        periods = start_periods.intersection(end_periods)

        if periods:
            if len(periods) > 1:
                periods = f'AND mv.period IN {str(tuple(periods))}'
            else:
                periods = f'AND mv.period = {list(periods)[0]}'
        else:
            periods = ''

        if accounts:
           if len(accounts) > 1:
               accounts = f' AND ml.account IN {str(accounts)}'
           else:
               accounts = f' AND ml.account = {accounts[0]}'

        args = {
            'company': data['company'],
            'periods': periods,
            'accounts': accounts
        }
        df_records = polar_query(move_general, args)
        records = df_records.rows(named=True)
        report_context['records'] = records
        report_context['company'] = company
        report_context['start_period'] = start_period.name if start_period else ""
        report_context['end_period'] = end_period.name if end_period else ""
        report_context['start_code'] = ''
        report_context['end_code'] = ''
        return report_context
