SELECT
    f.name AS name,
    a.code AS code,
    a.name AS account_name,
    SUM(CASE WHEN l.maturity_date >= '2024-05-01' AND l.maturity_date <= '2024-05-30' THEN l.debit - l.credit ELSE 0 END) AS "1"
FROM
    account_account_cashflow AS f
LEFT JOIN "account_cashflow-account_account" AS af ON f.id = af.cash_flow
LEFT JOIN account_move_line AS l ON af.account = l.account
LEFT JOIN account_move AS m ON l.move = m.id
LEFT JOIN account_account AS a ON l.account = a.id
WHERE
    f.parent IS NOT NULL
    AND f.id = 2
GROUP BY
    f.name, a.code, a.name
ORDER BY f.name, a.code
