# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime
from dateutil import tz

from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pool import PoolMeta


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    manager = fields.Many2One('party.party', 'Manager')
    accountant = fields.Many2One('party.party', 'Accountant')
    logo = fields.Binary('Logo')

    @classmethod
    def convert_timezone(cls, date_time, to_utc=False, company_id=None):
        if not company_id:
            company_id = Transaction().context.get('company')

        company = cls(company_id)
        if company.timezone:
            if to_utc:
                from_zone = tz.gettz(company.timezone)
                to_zone = tz.gettz('UTC')
            else:
                from_zone = tz.gettz('UTC')
                to_zone = tz.gettz(company.timezone)
            date_time = date_time.replace(tzinfo=from_zone)
            return date_time.astimezone(to_zone)
        else:
            return date_time

    @classmethod
    def time_now(cls):
        now = datetime.now()
        return cls.convert_timezone(now).strftime("%d-%m-%Y %H:%M %p")
